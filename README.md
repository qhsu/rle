# README #

This is the source code used by the third year Chemistry students at the University of British Columbia for experiment A-08, 
the detection of the concentration of iron via the oxidation of luminol and peroxide via chemiluminescence detected by
a photomultiplier tube.  This software was developed for CHEM 445, a lab research project and was for class credit.

### How do I get set up? ###

* This software was written to work with 64-bit Labview.
* This software requires the download of the JKI QSM modules and related software.
* A configuration file is provided to configure the parameters of the DAQ to whichever channel the signal has been connected to.

### Who do I talk to? ###

* This project is no longer being maintained.  Any enquiries will not be replied to as I no longer have a valid Labview liscence.